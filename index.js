import EventEmitter from "external:@totates/events/v1";

/*
import loglevel from "loglevel";
import prefix from "loglevel-plugin-prefix";
prefix.reg(loglevel);
prefix.apply(loglevel, {
    template: "[%n] %l:",
    levelFormatter: level => level.toUpperCase(),
    nameFormatter: name => name || "root"
});
export default loglevel;
*/

// TODO: static fields one day
const LEVELS = ["TRACE", "DEBUG", "INFO", "WARN", "ERROR"];
const DEFAULT = LEVELS.indexOf("WARN");

// TODO: static method one day
function getRank(key) {
    const level = window.localStorage[key];
    if (level) {
        const rank = LEVELS.indexOf(level);
        if (rank !== -1)
            return rank;

        delete window.localStorage[key];
        console.warn(`logging: cleared invalid level "${key}"`);
    }
    return null;
}

class Logger {
    constructor(parent, name) {
        this.parent = parent; // TODO: weakref
        this.name = name;
        LEVELS.forEach((LEVEL, rank) => {
            const level = LEVEL.toLowerCase();
            const method = level === "debug" ? "log" : level;
            const prefix = `[${this.name}] ${level.toUpperCase()}:`;
            this[level] = (...args) => {
                if (rank >= (getRank(this.key) || getRank("logging") || DEFAULT)) {
                    console[method](prefix, ...args);
                    this.parent.emit(level, this, args);
                }
            };
        });
    }

    get key() {
        return `@totates/logging:${this.name}`;
    }

    setLevel(level) {
        const rank = LEVELS.indexOf(level);
        if (rank !== -1) {
            if (level === "WARN")
                window.localStorage.removeItem(this.key);
            else
                window.localStorage[this.key] = level;
        }
        else
            console.warn(`logging: invalid level "${level}"`);
    }
}

class Logging extends EventEmitter {
    constructor() {
        super();
        this.loggers = {};
    }
    getLogger(name) {
        let logger = this.loggers[name];
        if (!logger)
            logger = this.loggers[name] = new Logger(this, name);
        return logger;
    }
}

const logging = new Logging();
export default logging;
